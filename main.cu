#include "ray.h"
#include "vec3.h"

#include <iostream>
#include <time.h>

#include <curand_kernel.h>

using namespace std;

#define checkCudaErrors(val) check_cuda( (val), #val, __FILE__, __LINE__ )
void check_cuda(cudaError_t result, char const *const func, const char *const file, int const line) {
    if (result) {
        cerr << "CUDA error = " << static_cast<unsigned int>(result) << " at " <<
        file << ":" << line << " '" << func << "' \n";
        // Make sure we call CUDA Device Reset before exiting
        cudaDeviceReset();
        exit(99);
    }
}

__device__ bool hit_sphere(const vec3& center, const float radius, const Ray& r) {
	vec3 oc = r.origin() - center;
	const float a = dot(r.direction(), r.direction());
	const float b = 2.0f * dot(oc, r.direction());
	const float c = dot(oc, oc) - radius * radius;
	const float d = b * b - 4.0f * a * c;
	return (d > 0.0f);
}

__device__ vec3 color(const Ray& r) {
	if (hit_sphere(vec3(0.0f, 0.0f, -1.0f), 0.5f, r)) {
		return vec3(1.0f, 0.0f, 0.0f);
	}

	const float t = 0.5f * (r.direction().y() + 1.0f);
	return (1.0f - t) * vec3(1.0f) + t * vec3(0.5f, 0.7f, 1.0f);
}

__global__ void render_init(int max_x, int max_y, curandState *rand_state) {
   int i = threadIdx.x + blockIdx.x * blockDim.x;
   int j = threadIdx.y + blockIdx.y * blockDim.y;
   if((i >= max_x) || (j >= max_y)) return;
   int pixel_index = j*max_x + i;
   //Each thread gets same seed, a different sequence number, no offset
   curand_init(1984, pixel_index, 0, &rand_state[pixel_index]);
}

__global__ void render(
	vec3* 			fb, 
	const int 		max_x, 
	const int 		max_y,
	const int 		ns,
	const vec3		lower_left_corner,
	const vec3		horizontal,
	const vec3		vertical,
	const vec3		origin,
	curandState *rand_state) {
	const int x = threadIdx.x + blockIdx.x * blockDim.x;
	const int y = threadIdx.y + blockIdx.y * blockDim.y;

	if ((x >= max_x) || (y >= max_y)) return;

	const int pi = y * max_x + x;

	vec3 pcolor(0.0f);

	for (int s = 0; s < ns; ++s) {
		const float u = float(x) / max_x;
		const float v = float(y) / max_y;
		const Ray r(origin, unit_vector(lower_left_corner + u * horizontal + v * vertical));
		pcolor += color(r);
	}

	fb[pi] = pcolor / float(ns);
}

int main() {
	const int nx = 1200;
	const int ny = 600;
	const int ns = 8;
	const int tx = 8;
	const int ty = 8;

	const int num_pixels = nx * ny;
	const size_t fb_size = num_pixels * sizeof(vec3);

	// Allocate the frame buffer.
	vec3* fb;
	checkCudaErrors(cudaMallocManaged((void **)&fb, fb_size));

	// Prepare rendering blocks.
	dim3 blocks(nx / tx + 1, ny / ty + 1);
	dim3 threads(tx, ty);

	// Init RNG.
	curandState *d_rand_state;
	checkCudaErrors(cudaMalloc((void **)&d_rand_state, num_pixels*sizeof(curandState)));

	render_init<<<blocks, threads>>>(nx, ny, d_rand_state);
	checkCudaErrors(cudaGetLastError());
	checkCudaErrors(cudaDeviceSynchronize());

	// Render it.
	render<<<blocks, threads>>>(
		fb, 
		nx, 
		ny,
		ns,
		vec3(-2.0, -1.0, -1.0),
		vec3(4.0, 0.0, 0.0),
		vec3(0.0, 2.0, 0.0),
		vec3(0.0, 0.0, 0.0),
		d_rand_state);

	checkCudaErrors(cudaGetLastError());
	checkCudaErrors(cudaDeviceSynchronize());

	// Output it.
	cout << "P3\n" << nx << " " << ny << "\n255\n";
	
	for (int y = ny - 1; y >= 0; --y) {
		for (int x = 0; x < nx; ++x) {
			const int pi = y * nx + x;
			vec3& color = fb[pi];
			const int ri = int(255.99f * color.e[0]);
			const int gi = int(255.99f * color.e[1]);
			const int bi = int(255.99f * color.e[2]);
			cout << ri << " " << gi << " " << bi << "\n";
		}
	}

	checkCudaErrors(cudaFree(fb));

	return 0;
}
